﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GlossaryNotes
{
	using GlossaryType = Dictionary<Keyword, List<Sentence>>;

	class Program
	{
		/** TODO:
		 *   file selector
		 *   handle exceptions
		 *   format output
		 *   improve parser and add error handling
		 *   latex packages in args?
		 *   search for all occurrences of each keyword (future)
		 *   extra markup to break document into multiple glossaries (or is this contrary to the idea of the project?)
		**/

		static void Main(string[] args)
		{
			var filePath = string.Empty;

			if (args.Length == 0)
			{
				filePath = GetPathInput();
			}
			else
			{
				if (File.Exists(args.First()))
				{
					filePath = args.First();
				}
				else
				{
					throw new FileNotFoundException();
				}
			}

			// parse
			var glossary = ParseNotes(File.ReadAllText(filePath));

			// output LaTeX
			File.WriteAllText(Path.ChangeExtension(filePath, "tex"), ToLaTeX(glossary));
		}


		static string GetPathInput()
		{
			System.Console.Write("File name:");

			var haveFile = false;
			var input = string.Empty;

			while (!haveFile)
			{
				input = System.Console.ReadLine();

				try
				{
					if (!Path.HasExtension(input))
					{
						input = Path.ChangeExtension(input, NoteExtension);
					}
				}
				catch { }

				if (File.Exists(input))
				{
					haveFile = true;
				}
				else
				{
					Console.Write("Full Path:");
				}
			}

			return input;
		}

		/// <summary>
		/// Takes input string of marked notes and builds data structure for marked notes.
		/// </summary>
		/// <param name="notes"></param>
		/// <returns></returns>
		static GlossaryType ParseNotes(string notes)
		{
			var glossary = new GlossaryType();
			var thing = notes.Split(NoteTerminators, StringSplitOptions.RemoveEmptyEntries).ToList();
			// split into sentences
			foreach (string sentence in notes.Split(NoteTerminators, StringSplitOptions.RemoveEmptyEntries).ToList())
			{
				var wordlist = new List<IWord>();

				var noteParts = Regex.Split(sentence, @"(?=" + KeywordOpen + ")(.*?)(?<=" + KeywordClose + ")").Where(s => !string.IsNullOrWhiteSpace(s)).ToList();

				// separate out marked words or terms
				foreach (string st in noteParts)
				{
					// add appropriate IWord to list
					if (st.StartsWith(KeywordOpen) && st.EndsWith(KeywordClose))
					{
						wordlist.Add(new Keyword { content = st.TrimStart(KeywordOpen.ToCharArray()).TrimEnd(KeywordClose.ToCharArray()).Trim().ToString() });
					}
					else
					{
						wordlist.Add(new Word { content = st.Trim() });
					}
					Debug.WriteLine(st.Trim());
				}

				// add new entry or new sentence to glossary (if entry key already exists)
				foreach (IWord w in wordlist)
				{
					if (w.GetType().Equals(typeof(Keyword)))
					{
						if (glossary.Keys.Contains((Keyword)w))
						{
							glossary[(Keyword)w].Add(new Sentence() { content = wordlist });
						}
						else
						{
							glossary.Add((Keyword)w, new List<Sentence>() { new Sentence() { content = wordlist } });
						}
					}
				}
			}

			return glossary;
		}

		/// <summary>
		/// Writes the note data to LaTeX code with a section for each keyword.
		/// Prettyprint TeX?
		/// </summary>
		/// <returns></returns>
		static string ToLaTeX(GlossaryType glossary)
		{
			List<string> str = new List<string>() { LaTeXPreamble };

			foreach (Keyword k in glossary.Keys.ToList())
			{
				// create section for each keyword
				str.Add("\\section{" + k.content + "}");
				str.Add("\\label{sec:" + k.content + "}");
				str.Add("\\begin{itemize}");

				foreach (Sentence s in glossary[k])
				{
					// create item for each sentence containing this keyword
					str.Add("\\item");

					foreach (var w in s.content)
					{
						if (w.GetType().Equals(typeof(Keyword)))
						{
							str.Add("\\hyperref[sec:" + w.content + "]{" + w.content + "}");
						}
						else
						{
							str.Add(w.content);
						}
					}
				}

				str.Add("\\end{itemize}");
			}

			str.Add(LaTeXPostamble);

			return string.Join(" ", str);
		}

		static readonly string LaTeXPreamble = "\\documentclass{article}\\usepackage{hyperref}\\begin{document}";
		static readonly string LaTeXPostamble = "\\end{document}";

		static readonly string KeywordOpen = "~!";
		static readonly string KeywordClose = "!~";

		static readonly string[] NoteTerminators = new string[] { ".", "\n", "\r", "\f" };

		static readonly string NoteExtension = "note";
	}

	interface IWord { string content { get; set; } }

	struct Word : IWord { public string content { get; set; } }

	struct Keyword : IWord { public string content { get; set; } }

	struct Sentence { public List<IWord> content { get; set; } }
}
